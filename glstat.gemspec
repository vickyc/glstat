
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = 'glstat'
  spec.version       = '0.1.0'
  spec.authors       = ['Vicky Chijwani']
  spec.email         = ['vickychijwani@gmail.com']

  spec.summary       = 'GitLab response timing utility'
  spec.description   = 'Command-line tool to check the status of https://about.gitlab.com
                        and report an average response time after probing the site every
                        10 seconds for one minute'
  spec.homepage      = 'https://gitlab.com/vickyc/glstat'
  spec.license       = 'MIT'

  spec.files         = Dir.glob('lib/**/*') + Dir.glob('bin/**/*') + %w(glstat.gemspec Gemfile Rakefile README.md)
  spec.bindir        = 'bin'
  spec.executables   = ['glstat']
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'pry', '~> 0.11.3'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
