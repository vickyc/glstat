require 'net/http'

module GLStat

  # Computes the average response time for a given URL
  class ResponseTimer

    attr_reader :url, :sample_count, :interval

    def initialize(url, sample_count, interval, logger)
      @url = url
      @sample_count = sample_count
      @interval = interval
      @logger = logger
    end

    def measure
      errors = 0
      response_times = (1..@sample_count).map do
        time, error = time_single
        if error.nil?
          response_time = time
          @logger.debug "Measured single response time = #{time} sec"
        else
          response_time = nil
          errors += 1
          @logger.debug "Error: #{error}"
        end
        wait_time = @interval - time
        if wait_time.positive?
          @logger.debug "Waiting #{wait_time} sec before the next request"
          sleep wait_time
        end
        response_time
      end.compact
      avg = time_average(response_times).round(2)
      { average: avg, errors: errors }
    end

    private
    def time_single
      error = nil
      time = Benchmark.realtime { error = try_http_get }
      [time, error]
    end

    def try_http_get
      Timeout.timeout(@interval) { Net::HTTP.get(URI.parse(@url)) }
      return nil
    rescue RuntimeError => e
      return e
    end

    def time_average(times)
      if times.empty?
        return 0
      end
      times.reduce(:+).to_f / times.size
    end

  end

end
