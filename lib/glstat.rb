require 'benchmark'
require 'http'
require 'logger'
require 'optparse'

require 'response_timer'

module GLStat

  DEFAULTS = {
      url: "https://about.gitlab.com",
      samples: 6,
      interval: 10,
      log_level: "info"
  }

  def self.run(args=[])
    opts = DEFAULTS.merge(parse_options(args))
    logger = Logger.new(STDOUT)
    logger.level = Logger::Severity.constants.map { |l| [l.to_s.downcase, l] }.to_h[opts[:log_level]]
    logger.debug("Running with arguments: #{opts}")

    results = ResponseTimer.new(opts[:url], opts[:samples], opts[:interval], logger).measure
    avg, errors = results[:average], results[:errors]
    case errors
      when 0
        puts "Response time: #{avg}s (#{opts[:samples]} samples)"
      when opts[:samples]
        puts "All #{opts[:samples]} requests failed"
      else
        puts "Response time: #{avg}s (#{errors} of #{opts[:samples]} requests failed)"
    end
  end

  def self.parse_options(args)
    opts = {}
    OptionParser.new do |parser|
      parser.banner = 'glstat - GitLab response timing utility'
      parser.on('-u', '--url URL', "Specify a URL to hit (default: #{DEFAULTS[:url]})") do |value|
        opts[:url] = value
      end
      parser.on('-s', '--samples SAMPLE_COUNT',
                "Number of samples to average (default: #{DEFAULTS[:samples]})") do |value|
        opts[:samples] = value.to_i
      end
      parser.on('-i', '--interval INTERVAL',
                "Interval between subsequent requests, in seconds (default: #{DEFAULTS[:interval]})") do |value|
        opts[:interval] = value.to_f
      end
      log_levels = Logger::Severity.constants.map(&:to_s).map(&:downcase).join(", ")
      parser.on('-l', '--log-level LEVEL', "Possible values: #{log_levels}") do |value|
        opts[:log_level] = value
      end
    end.parse(args)
    opts
  end

end
