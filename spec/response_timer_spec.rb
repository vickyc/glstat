describe GLStat do
  describe 'measure' do
    let(:url) { "https://gitlab.com" }
    let(:logger) { l = Logger.new(STDOUT); l.level = Logger::Severity::INFO; l }

    it 'should return the average response time to at most 2 decimal places' do
      timer = GLStat::ResponseTimer.new(url, 3, 0, logger)
      stub_timer_samples(timer, [3, 1, 6])
      expect(timer.measure).to include average: 3.33
    end

    context 'when requests fail' do
      let(:timer) { GLStat::ResponseTimer.new(url, 5, 0, logger) }

      it 'should ignore failed requests when computing the average' do
        stub_timer_samples(timer, [3, 1, [0, Timeout::Error.new], 5, [0, RuntimeError.new]])
        expect(timer.measure).to include average: 3, errors: 2
      end

      it 'should return an average of 0 if all requests fail' do
        stub_timer_samples(timer, [[0, Timeout::Error.new]]*timer.sample_count)
        expect(timer.measure).to include average: 0
      end
    end

  end
end
