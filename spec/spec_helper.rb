require "bundler/setup"
require "glstat"

# stubs ResponseTimer::time_single so it can return a value or raise a given error on consecutive calls
def stub_timer_samples(timer, results)
  allow(timer).to receive(:time_single) do
    case r = results.shift
      when Array then r
      when Fixnum then [r, nil]
      else raise "Unable to handle result #{r}"
    end
  end
end
