# GLStat

GLStat is a GitLab response timing utility. It is a command-line tool to check the status of
https://about.gitlab.com and report an average response time after probing the site every
10 seconds for one minute.

## Usage

- `glstat` with no arguments - default functionality, described above
- `glstat -l debug` - see more logs as it executes
- `glstat --help` - for more options

## Development

- `bundle` - install dependencies, including those required for development
- `rake install` - install the `glstat` binary along with dependencies
- `rake spec` - run tests
- `rake run` - run the development version of the gem with default arguments
- `rake console` - start a pry console with the gem loaded
